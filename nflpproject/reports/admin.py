from django.contrib import admin
from reports.models import Stadium
from reports.models import Team
from reports.models import Position
from reports.models import Player
from reports.models import Transfer
from reports.models import Dismiss

# Register your models here.
admin.site.register(Stadium)
admin.site.register(Team)
admin.site.register(Position)
admin.site.register(Player)
admin.site.register(Transfer)
admin.site.register(Dismiss)
