from django.urls import path, include
from reports import views

urlpatterns = [
    path('', views.index, name='reportsindex'),
    path('players/', views.PlayerListView.as_view(), name='players-list'),
    path('stadiums/', views.StadiumListView.as_view(), name='stadiums-list'),
    path('teams/', views.TeamListView.as_view(), name='teams-list'),
    path('player/<int:pk>/', views.PlayerDetailView.as_view(), name='player-details'),
    path('stadium/<int:pk>/', views.StadiumDetailView.as_view(), name='stadium-details'),
    path('team/<int:pk>/', views.TeamDetailView.as_view(), name='team-details'),
    path('transfers/', views.TransferListView.as_view(), name='transfers-list'),
    path('dismisses/', views.DismissListView.as_view(), name='dismisses-list'),
    path('team-players/<int:team>', views.teamplayers, name='team-players-list'),
]
