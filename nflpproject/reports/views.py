from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from reports.models import Stadium
from reports.models import Team
from reports.models import Position
from reports.models import Player
from reports.models import Transfer
from reports.models import Dismiss

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'reports/index.html',context)

def teamplayers(request,team):
    queryset = Player.objects.filter(team=team)
    context = {
        'team_players':queryset
    }
    return render(request,'reports/team-players.html',context)


class PlayerListView(ListView):
    model = Player
    template_name = 'reports/players-list.html'
    paginate_by = 10  # if pagination is desired

    queryset = Player.objects.all()

class PlayerDetailView(DetailView):
    model = Player
    template_name = 'reports/player-details.html'

class StadiumListView(ListView):
    model = Stadium
    template_name = 'reports/stadiums-list.html'
    paginate_by = 10  # if pagination is desired

    queryset = Stadium.objects.all()

class StadiumDetailView(DetailView):
    model = Stadium
    template_name = 'reports/stadium-details.html'

class TeamListView(ListView):
    model = Team
    template_name = 'reports/teams-list.html'
    paginate_by = 10  # if pagination is desired

    queryset = Team.objects.all()

class TeamDetailView(DetailView):
    model = Team
    template_name = 'reports/team-details.html'

class TransferListView(ListView):
    model = Transfer
    template_name = 'reports/transfers-list.html'
    paginate_by = 10  # if pagination is desired

    queryset = Transfer.objects.all()

class DismissListView(ListView):
    model = Dismiss
    template_name = 'reports/dismiss-list.html'
    paginate_by = 10  # if pagination is desired

    queryset = Dismiss.objects.all()
