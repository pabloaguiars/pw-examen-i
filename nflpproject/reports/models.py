from django.db import models

# Create your models here.

class Stadium(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    slug = models.SlugField()

class Team(models.Model):
    stadium = models.ForeignKey(Stadium, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    slug = models.SlugField()

class Position(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=144)
    slug = models.SlugField()

class Player(models.Model):
    name = models.CharField(max_length=50)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    slug = models.SlugField()

class Transfer(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team_out = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='team_out')
    team_in = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='team_in')
    date = models.DateField()

class Dismiss(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    date = models.DateField()
